import { BsBell } from "react-icons/bs";
import styles from "./Notification.module.css";

const Notification = () => {
  return (
    <div className={`${styles.notif}`}>
      <BsBell />
    </div>
  );
};

export default Notification;
